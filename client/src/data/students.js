import student_list from './students.json';
// function that used in the student_main_list

const randomStudentList = (a) => { // Fisher-Yates shuffle, no side effects
                                   // make the student list with random arrange
  let i = a.length,
      t, j;
  a = a.slice();
  if (i === 0) {
    return []
  }
  while (--i) {
    t = a[i];
    a[i] = a[j = ~~(Math.random() * (i + 1))];
    a[j] = t
  }
  return a
};

let student_main_list = randomStudentList(student_list); //make the list rendered randomly

let cities = student_list.map(student => student.city);
cities = cities.filter((city, index) => cities.indexOf(city) === index).sort();
export default student_main_list;
export {
  cities,
};

