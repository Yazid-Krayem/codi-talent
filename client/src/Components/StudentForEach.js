// modules imports
import React from 'react';
import { Button, Thumbnail, Col } from 'react-bootstrap';
// files imports
import ContactFormModal from './ContactFormModal';

// return ONE student with structure
const StudentForEach = ({displayName, skills, city, OnClickAdd, image, linkedIn, lookingFor}) => (
    <div style={{minWidth: 300}} className={'student-item'}>
      <div className={'thumbnail'}>
        <div className="caption">
          <img src={image} width="150px" height="150px" circle={'true'} alt={displayName + ' Student'}/>
          <div className='student-info'>
            <h2 className="student-name">{displayName}</h2>
            <p style={{fontSize: '20px'}}>{city}</p>
            <h3 className='student-skills'>Tech skills</h3>
            <div className="skills">
              <p>#{skills.join(' #')}</p>
            </div>
          </div>
          
          <div className="contact-info">
            <hr/>
            <ContactFormModal name={displayName}/>
            {
              linkedIn ? <a target="_blank" href={'//www.linkedIn.com/in/' + linkedIn} style={{fontSize: '37px'}}><i
                      className={'fa fa-linkedin-square'}/></a> :
                  <i className={'fa fa-linkedIn-square'} style={{fontSize: '37px'}}/>
            }
            <div>
              <Button bsStyle="success" onClick={OnClickAdd}>Shortlist</Button>
            </div>
          </div>
        
        </div>
      </div>
    </div>
);

export default StudentForEach;
